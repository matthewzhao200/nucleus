#include <stdlib.h>
#include <wayland-server-core.h>
#include <wlr/util/log.h>
#include <wlr/types/wlr_xdg_shell.h>

#include "server.h"
#include "view.h"

static void handle_map(struct wl_listener *listener, void *data);
static void handle_unmap(struct wl_listener *listener, void *data);
static void handle_move(struct wl_listener *listener, void *data);
static void handle_resize(struct wl_listener *listener, void *data);
static void handle_destroy(struct wl_listener *listener, void *data);
static bool view_at(struct nc_view *view, double lx, double ly,
        struct wlr_surface **surface, double *sx, double *sy);
static void begin_interactive(struct nc_view *view,
        enum nc_cursor_mode mode, uint32_t edges);

void handle_new_xdg_surface(struct wl_listener *listener, void *data) {
    wlr_log(WLR_INFO, "Creating new xdg surface");
    struct nc_server *server = wl_container_of(listener, server,
            listen_new_xdg_surface);
    struct wlr_xdg_surface *xdg_surface = data;
    if (xdg_surface->role != WLR_XDG_SURFACE_ROLE_TOPLEVEL) {
        return;
    }

    struct nc_view *view = calloc(1, sizeof(struct nc_view));
    view->server = server;
    view->xdg_surface = xdg_surface;
    view->mapped = false;
    view->x = 0, view->y = 0;

    view->listen_map.notify = handle_map;
    wl_signal_add(&xdg_surface->events.map, &view->listen_map);
    view->listen_unmap.notify = handle_unmap;
    wl_signal_add(&xdg_surface->events.unmap, &view->listen_unmap);
    view->listen_destroy.notify = handle_destroy;
    wl_signal_add(&xdg_surface->events.destroy, &view->listen_destroy);

    struct wlr_xdg_toplevel *toplevel = xdg_surface->toplevel;
    view->listen_move.notify = handle_move;
    wl_signal_add(&toplevel->events.request_move, &view->listen_move);
    view->listen_resize.notify = handle_resize;
    wl_signal_add(&toplevel->events.request_resize, &view->listen_resize);

    wl_list_insert(&server->views, &view->link);
}

static void handle_map(struct wl_listener *listener, void *data) {
    struct nc_view *view = wl_container_of(listener, view, listen_map);
    view->mapped = true;

    struct nc_server *server = view->server;
    struct wlr_keyboard *keyboard = wlr_seat_get_keyboard(server->seat->wlr_seat);
    wlr_seat_keyboard_notify_enter(server->seat->wlr_seat, view->xdg_surface->surface,
            keyboard->keycodes, keyboard->num_keycodes,
            &keyboard->modifiers);
}

static void handle_unmap(struct wl_listener *listener, void *data) {
    struct nc_view *view = wl_container_of(listener, view, listen_unmap);
    view->mapped = false;
}

static void handle_move(struct wl_listener *listener, void *data) {
    struct nc_view *view = wl_container_of(listener, view, listen_move);

    begin_interactive(view, NC_CURSOR_MODE_MOVE, 0);
}

static void handle_resize(struct wl_listener *listener, void *data) {
    struct nc_view *view = wl_container_of(listener, view, listen_resize);
    struct wlr_xdg_toplevel_resize_event *event = data;

    begin_interactive(view, NC_CURSOR_MODE_RESIZE, event->edges);
}

static void handle_destroy(struct wl_listener *listener, void *data) {
    wlr_log(WLR_INFO, "Destroying");
    struct nc_view *view = wl_container_of(listener, view, listen_destroy);

    wl_list_remove(&view->listen_map.link);
    wl_list_remove(&view->listen_unmap.link);
    wl_list_remove(&view->listen_move.link);
    wl_list_remove(&view->listen_resize.link);
    wl_list_remove(&view->listen_destroy.link);

    wl_list_remove(&view->link);

    free(view);
}

struct nc_view *nc_desktop_view_at(struct nc_server *server, double lx, double ly,
        struct wlr_surface **surface, double *sx, double *sy) {
    struct nc_view *view;
    wl_list_for_each(view, &server->views, link) {
        if (view_at(view, lx, ly, surface, sx, sy)) {
            return view;
        }
    }

    return NULL;
}

static bool view_at(struct nc_view *view, double lx, double ly,
        struct wlr_surface **surface, double *sx, double *sy) {
    double view_sx = lx - view->x;
    double view_sy = ly - view->y;

    struct wlr_surface_state *state = &view->xdg_surface->surface->current;

    double _sx, _sy;
    struct wlr_surface *_surface = NULL;
    _surface = wlr_xdg_surface_surface_at(
            view->xdg_surface, view_sx, view_sy, &_sx, &_sy);

    if (_surface != NULL) {
        *sx = _sx;
        *sy = _sy;
        *surface = _surface;
        return true;
    }

    return false;
}

void nc_focus_view(struct nc_view *view, struct wlr_surface *surface) {
    if (view == NULL) {
        return;
    }

    struct nc_server *server = view->server;
    wl_list_remove(&view->link);
    wl_list_insert(&server->views, &view->link);

    wlr_xdg_toplevel_set_activated(view->xdg_surface, true);
}

static void begin_interactive(struct nc_view *view,
        enum nc_cursor_mode mode, uint32_t edges) {
    struct nc_server *server = view->server;

    struct wlr_surface *focused_surface =
        server->seat->wlr_seat->pointer_state.focused_surface;
    if (view->xdg_surface->surface != focused_surface) {
        // client isn't focused, don't accept request
        return;
    }

    server->grab_state.view = view;
    server->seat->cursor_mode = mode;

    switch (mode) {
    case NC_CURSOR_MODE_MOVE:
        server->grab_state.grab_x = server->seat->cursor->x - view->x;
        server->grab_state.grab_y = server->seat->cursor->y - view->y;
        break;
    case NC_CURSOR_MODE_RESIZE:;
        struct wlr_box geo_box;
        wlr_xdg_surface_get_geometry(view->xdg_surface, &geo_box);

        double border_x = (view->x + geo_box.x) + ((edges & WLR_EDGE_RIGHT) ? geo_box.width : 0);
        double border_y = (view->y + geo_box.y) + ((edges & WLR_EDGE_BOTTOM) ? geo_box.height : 0);
        server->grab_state.grab_x = server->seat->cursor->x - view->x;
        server->grab_state.grab_y = server->seat->cursor->y - view->y;

        server->grab_state.box = geo_box;
        server->grab_state.box.x += view->x;
        server->grab_state.box.y += view->y;

        server->grab_state.resize_edges = edges;
        /*printf("%d %d %d %d\n", edges & WLR_EDGE_TOP, edges & WLR_EDGE_RIGHT,*/
                /*edges & WLR_EDGE_BOTTOM, edges & WLR_EDGE_LEFT);*/
        break;
    default:
        break;
    }
}

void nc_move_view(struct nc_view *view, double x, double y) {
    view->x = x;
    view->y = y;
}
