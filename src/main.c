#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wlr/util/log.h>

#include "server.h"

int main(void) {
    // Since wayland requires XDG_RUNTIME_DIR to be set, abort with just the
    // clear error message.
    if (!getenv("XDG_RUNTIME_DIR")) {
        fprintf(stderr,
                "XDG_RUNTIME_DIR is not set in the environment. Aborting.\n");
        exit(EXIT_FAILURE);
    }

    wlr_log_init(WLR_DEBUG, NULL);

    struct nc_server server;
    if (!nc_server_init(&server)) {
        return 1;
    }

    if (!nc_server_start(&server)) {
        return 1;
    }

    setenv("WAYLAND_DISPLAY", server.socket, true);
    setenv("GDK_BACKEND", "wayland", true);
    if (fork() == 0) {
        execl("/bin/sh", "/bin/sh", "-c", "termite");
    }
    /*if (fork() == 0) {*/
        /*execl("/bin/sh", "/bin/sh", "-c", "pavucontrol");*/
    /*}*/

    nc_server_run(&server);
    nc_server_destroy(&server);

    return 0;
}
