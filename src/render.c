#define _POSIX_C_SOURCE 200809L
#include <wlr/util/log.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_matrix.h>
#include <wlr/types/wlr_output.h>

#include "server.h"
#include "output.h"
#include "view.h"
#include "render.h"

static void render_surface(struct wlr_surface *surface,
        int sx, int sy, void *data);

void nc_render_frame(struct nc_output *output) {
    struct wlr_renderer *renderer = output->server->renderer;

    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);

    // wlr_output_attach_render makes the OpenGL context current.
	if (!wlr_output_attach_render(output->wlr_output, NULL)) {
		return;
	}
	// The "effective" resolution can change if you rotate your outputs.
	int width, height;
	wlr_output_effective_resolution(output->wlr_output, &width, &height);

	// Begin the renderer (calls glViewport and some other GL sanity checks)
	wlr_renderer_begin(renderer, width, height);

    // clear the screen
    float color[4] = {0.3, 0.3, 0.3, 1.0}; // RGBA
    wlr_renderer_clear(renderer, color);

    struct nc_view *view;
    wl_list_for_each_reverse(view, &output->server->views, link) {
        if (!view->mapped) {
            // don't render unmapped views
            continue;
        }

        struct nc_render_data rdata = {
            .output = output->wlr_output,
            .view = view,
            .renderer = renderer,
            .when = &now,
        };
        wlr_xdg_surface_for_each_surface(view->xdg_surface,
                render_surface, &rdata);
    }

    // render software cursors (only runs if hardware cursor support isn't available)
    wlr_output_render_software_cursors(output->wlr_output, NULL);

    // end and commit frame
    wlr_renderer_end(renderer);
    wlr_output_commit(output->wlr_output);
}

static void render_surface(struct wlr_surface *surface,
        int sx, int sy, void *data) {
    struct nc_render_data *rdata = data;
    struct nc_view *view = rdata->view;
    struct wlr_output *output = rdata->output;

    // obtain texture to render from surface
    struct wlr_texture *texture = wlr_surface_get_texture(surface);
    if (texture == NULL) {
        return;
    }

    // map global position in output layout to output-local position
    double ox = 0, oy = 0;
    wlr_output_layout_output_coords(
            view->server->output_layout, output, &ox, &oy);
    ox += view->x + sx;
    oy += view->y + sy;

    struct wlr_box box = {
        .x = ox * output->scale,
        .y = oy * output->scale,
        .width = surface->current.width * output->scale,
        .height = surface->current.height * output->scale,
    };

    // map out & render
    float matrix[9];
    enum wl_output_transform transform =
        wlr_output_transform_invert(surface->current.transform);
    wlr_matrix_project_box(matrix, &box, transform, 0,
            output->transform_matrix);

    wlr_render_texture_with_matrix(rdata->renderer, texture, matrix, 1.0);

    wlr_surface_send_frame_done(surface, rdata->when);
}
