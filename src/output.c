#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <wayland-server-core.h>
#include <wlr/util/log.h>
#include <wlr/types/wlr_output.h>

#include "server.h"
#include "render.h"
#include "output.h"

static struct nc_output *nc_output_create(struct wlr_output *wlr_output);
static void handle_frame(struct wl_listener *listener, void *data);
static void handle_destroy(struct wl_listener *listener, void *data);

static struct nc_output *nc_output_create(struct wlr_output *wlr_output) {
    // create an nc_output
    struct nc_output *output = calloc(1, sizeof(struct nc_output));
    output->wlr_output = wlr_output;

    // setup the callbacks that we want to get notified for
    output->listen_frame.notify = handle_frame;
    wl_signal_add(&output->wlr_output->events.frame, &output->listen_frame);
    output->listen_destroy.notify = handle_destroy;
    wl_signal_add(&output->wlr_output->events.destroy, &output->listen_destroy);

    return output;
}

void handle_new_output(struct wl_listener *listener, void *data) {
    wlr_log(WLR_INFO, "Creating new output");
    struct nc_server *server = wl_container_of(listener, server, listen_new_output);

    struct wlr_output *wlr_output = data;
    struct nc_output *output = nc_output_create(wlr_output);

    if (!wl_list_empty(&wlr_output->modes)) {
        struct wlr_output_mode *mode = wlr_output_preferred_mode(wlr_output);
        wlr_output_set_mode(wlr_output, mode);
    }

    clock_gettime(CLOCK_MONOTONIC, &output->last_frame);
    output->server = server;

    wl_list_insert(&server->outputs, &output->link);

    wlr_output_layout_add_auto(server->output_layout, wlr_output); // add the output to the layout
}

// called every time the output is ready to render a frame
static void handle_frame(struct wl_listener *listener, void *data) {
    struct nc_output *output = wl_container_of(listener, output, listen_frame);

    nc_render_frame(output);
}

// triggered when output is destroyed
static void handle_destroy(struct wl_listener *listener, void *data) {
    wlr_log(WLR_INFO, "Destroying output");
    struct nc_output *output = wl_container_of(listener, output, listen_destroy);

    // clean up
    wl_list_remove(&output->link);
    wl_list_remove(&output->listen_destroy.link);

    free(output);
}
