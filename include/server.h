#ifndef _NC_SERVER_H
#define _NC_SERVER_H

#include <wayland-server-core.h>
#include <wlr/backend.h>
#include <wlr/render/wlr_renderer.h>
#include <wlr/types/wlr_output_layout.h>

#include "seat.h"
#include "view.h"

struct nc_server {
    struct wl_display *wl_display;
    struct wlr_backend *backend;
    struct wlr_renderer *renderer;
    const char *socket;

    struct nc_seat *seat;
    struct wlr_output_layout *output_layout;
    struct wlr_xdg_shell *xdg_shell;

    struct wl_listener listen_new_output;
    struct wl_listener listen_new_xdg_surface;
    
    struct wl_list outputs; // nc_output::link
    struct wl_list views; // nc_view::link

    struct nc_view_grab_state grab_state;
};

bool nc_server_init(struct nc_server *server);
bool nc_server_start(struct nc_server *server);
void nc_server_run(struct nc_server *server);
void nc_server_destroy(struct nc_server *server);

#endif
