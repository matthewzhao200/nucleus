#ifndef _NC_VIEW_H
#define _NC_VIEW_H

#include <wayland-server-core.h>
#include <wlr/types/wlr_xdg_shell.h>

struct nc_view {
    struct nc_server *server;
    struct wlr_xdg_surface *xdg_surface;

    struct wl_listener listen_map;
    struct wl_listener listen_unmap;
    struct wl_listener listen_move;
    struct wl_listener listen_resize;
    struct wl_listener listen_destroy;

    bool mapped;
    int x, y;

    struct wl_list link; // nc_server::views
};

struct nc_view_grab_state {
    struct nc_view *view;
    double grab_x, grab_y;
    struct wlr_box box;
    uint32_t resize_edges;
};

void handle_new_xdg_surface(struct wl_listener *listener, void *data);
struct nc_view *nc_desktop_view_at(struct nc_server *server, double lx, double ly,
        struct wlr_surface **surface, double *sx, double *sy);
void nc_focus_view(struct nc_view *view, struct wlr_surface *surface);
void nc_move_view(struct nc_view *view, double x, double y);

#endif
